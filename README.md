ARLO Deployment
===============

This repository contains two components; a SaltStack configuration for deploying the ARLO project, and a Vagrant configuration for automating the build in a VirtualBox VM. 

You will need Vagrant installed on the machine you are attempting to use this on (presumably your local laptop or PC) along with VirtualBox or another Vagrant-compatible provider. 

Vagrant manages the provisioning of a new VM, downloading the base Ubuntu image, and installing SaltStack on the VM. 

Once provisioned, SaltStack manages the configuration of the VM, downloading, installing, and configuring all necessary services to run ARLO. 

This build is currently targeted at Ubuntu 12.04 distributions. Other distributions will require significant work for support, though I do intend to support this on at least CentOS in the future. 

This build is largely aimed towards developers of ARLO to have a convenient local system that is easy to recreate from a working configuration. 

Using Vagrant
-------------

To build a VirtualBox VM with an ARLO deploy, enter the same directory that 
contains this README file and execute 'vagrant up'. If successful, once 
complete the new VM will contain a full installation of ARLO. 


Networking and Hostnames
------------------------

As a convenience \*.vagrantdev.arloproject.com resolves to 192.168.99.9, the 
default IP for the Vagrant VM. This allows for a convenient FQDN that can be 
used by anyone locally.


Using SaltStack to Deploy
-------------------------

If using Vagrant, the SaltStack configuration will automatically be ran 
on the new VM. However, this same configuration can be used on any non-Vagrant system. 

To manually deploy directly on a host (NOTE: This makes significant changes 
to the system configuration, and should be used on a fresh server not running 
other services.):

* the minion, pillar/, and salt/ files need to be copied into /srv/

* bootstrap Salt 

    sudo wget -O - http://bootstrap.saltstack.org | sudo sh

* execute the deploy 

    sudo salt-call -l debug --local state.highstate

Post-Install Steps
------------------

### User Creation

After first building your new VM, you'll still need to manually add an ARLO Admin User: 

* Become ARLO user

    vagrant ssh

    sudo -u arlo -i

* Create ARLO Admin User

    cd /opt/arlo/nester

    source ../nester-virtualenv/bin/activate

    python manage.py createsuperuser

    mkdir -p /opt/arlo/user-files/{{USERNAME}}/{cache/segments,cache/jpgs,features,wav}

* Adding UserSettings to the Admin User (replaceing the appropriate username below):

    python manage.py shell

    from tools.models import UserSettings

    from django.contrib.auth.models import User

    user = User.objects.get(username='arlo')

    UserSettings(user=user).save()

* Login to ARLO as Admin, and create new user:

    http://vagrantdev.arloproject.com/tools/userHome


### Configuring Adapt API

Now we'll need to setup the ARLO internal System user and configure the
authentication settings in the Adapt Settings. First, we need to generate
an API Token for the Admin user. The easiest way is through the Django
Admin interface.

* Navigate to http://vagrantdev.arloproject.com/admin/ (or the appropriate
  URL for your installation).

* Click on 'Tokens'

* Click 'Add Token'

* Select the Admin user you previously selected.

* Select 'Save', and copy the Token for the next step.

Once we have the Token, we can either

* Update the Pillar settings in Salt and re-run the provisioning,
  especially useful for a permanent installation.

* Manually update the ArloSettings.properties files in /opt/arlo/adapt/
  *Note* that these changes will be overwritten with every invocation
  of SaltStack provisioning.

    vim /opt/arlo/adapt/ArloSettings-pool.properties 

    vim /opt/arlo/adapt/ArloSettings-QueueRunner.properties 

### Starting ARLO Adapt


* Become ARLO user

    vagrant ssh

    sudo -u arlo -i

* Nester (web) should already be running

* Build and Start ADAPT Java Pool and QueueRunner

    cd /opt/arlo

    ./build-java.sh

    ./start-java.sh

* If you want to tail the Java logs: 

    tail -f /opt/arlo/log/arlo.java-*current.out

### (Optional) Configuring Database Access to enable Testing

This step is optional to allow running the automated tests through Django's
testing framework. The 'arlo' database user needs to be configured with
superuser privileges in order to create and destroy the test database.

* Login to Vagrant and sudo up to root

    vagrant ssh

    sudo -i

* Run as root (Caution: if the browser renders these as smart quotes, this
  command may fail - on success, there should be no output):

    mysql -e 'GRANT ALL PRIVILEGES ON \*.\* TO "arlo"@"localhost" IDENTIFIED BY "password" WITH GRANT OPTION;'  # Grant the 'arlo' user superuser privileges.

    exit # logout of root


Security
--------

This installation makes no significant attempts at security. Some things to 
consider:

* MySQL root user will by default have no password
* ARLO database credentials will be readable in the Salt Pillar settings file


Troubleshooting
---------------

#### Salt Provisioning Failed

Automatic provisioning may have failed while creating the VM, or may need 
to be periodically refreshed. To re-run Salt provisioning, from the host
computer execute

    vagrant provision

Alternatively, if Salt has successfully installed, it can be executed by the
root user from the VM.

    # salt-call --local state.highstate


