
include:
{% if 'adapt' in grains['roles'] %}
  - .adapt
  - .aparapi
  - .java
{% endif %}
{% if 'nester' in grains['roles'] %}
  - .nester
  - .nester.apache
  - .firewall
{% endif %}
{% if 'database' in grains['roles'] %}
  - .database
{% endif %}

###
# Install on all servers

arlo-user:
  user.present:
    - name: {{ pillar["arlo_user"] }}
    - shell: /bin/bash
    - optional_groups: 
      - admin
      - sudo

arlo-path:
  file.directory:
    - require: 
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770

arlo-log-dir:
  file.directory:
    - require:
      - user: arlo-user
    - name: {{ pillar["arlo_log_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770
    - makedirs: True

arlo-userfile-path:
  file.directory:
    - require: 
      - user: arlo-user
    - name: {{ pillar["arlo_user_files_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770


###
# Install Adapt

{% if 'adapt' in grains['roles'] %}

{% endif %}

###
# Install Nester

{% if 'nester' in grains['roles'] %}

arlo-hostname:
  host.present:
    - ip: 0.0.0.0
    - names:
      - {{ pillar['arlo_hostname'] }}

arlo-media-hostname:
  host.present:
    - ip: 0.0.0.0
    - names:
      - {{ pillar['arlo_media_hostname'] }}

arlo-userfiles-media-link:
  file.symlink:
    - require:
      - file: arlo-userfile-path
      - git: nester
    - name: {{ pillar['nester_media_path'] }}/files/user-files
    - target: {{ pillar['arlo_user_files_path'] }}

{% endif %}