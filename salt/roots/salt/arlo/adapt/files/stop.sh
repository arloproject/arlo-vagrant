#!/bin/bash

CWD="{{ pillar['arlo_base_path'] }}"

PY_PID="${CWD}/python.pid"
JAVA_PID="${CWD}/java.pid"
QR_PID="${CWD}/java-queuerunner.pid"
PYTHON_QR_PID="${CWD}/python-queuerunner.pid"

# Python Daemons

if [ -f ${PY_PID} ]
then
	pkill -P `cat ${PY_PID}`
	rm ${PY_PID}
else
	echo "Python PID File does not exist!"
fi

if [ -f ${PYTHON_QR_PID} ]
then
	pkill -P `cat ${PYTHON_QR_PID}`
	rm ${PYTHON_QR_PID}
else
	echo "Python QueueRunner PID File does not exist!"
fi

# Java / Adapt Daemons

if [ -f $JAVA_PID ]
then
	#pkill -P `cat ${JAVA_PID}`
	kill `cat ${JAVA_PID}`
	rm ${JAVA_PID}
else
	echo "JAVA PID File does not exist!"
fi

if [ -f $QR_PID ]
then
	kill `cat ${QR_PID}`
	rm ${QR_PID}
else
	echo "JAVA QueueRunner PID File does not exist!"
fi

for ADAPT_PORT in {% for port in pillar['java_pool_instance_ports'] %}{{ port }} {% endfor %}
do
	JAVA_PID="${CWD}/java-port${ADAPT_PORT}.pid"
	if [ -f $JAVA_PID ]
	then
		kill `cat ${JAVA_PID}`
		rm ${JAVA_PID}
	else
		echo "JAVA PID File Does not exist! (${JAVA_PID})"
	fi
done
