#!/bin/bash

##################################################
# Post-Jython Version - separate java and python #

HOST="{{ pillar['arlo_hostname'] }}"

# directory contains nester, adapt, log, etc.
CWD="{{ pillar['arlo_base_path'] }}"

NOW=$(date +"%d%b%y.%H%M%S")

###
# Java Settings

# Memory
JAVA_MAX_HEAP="{{ pillar['adapt_java_max_heap'] }}"

# Garbage Collector Settings
GC_SETTINGS=""
GC_SETTINGS="${GC_SETTINGS} -XX:MaxHeapFreeRatio=20"
GC_SETTINGS="${GC_SETTINGS} -XX:MinHeapFreeRatio=5"
GC_SETTINGS="${GC_SETTINGS} -Xincgc"
GC_SETTINGS="${GC_SETTINGS} -verbose:gc"
GC_SETTINGS="${GC_SETTINGS} -XX:+PrintGCDetails"

# APARAPI Settings
APARAPI_SETTINGS="-Djava.library.path={{ pillar['aparapi_folder'] }} "
APARAPI_SETTINGS="${APARAPI_SETTINGS} -Dcom.amd.aparapi.executionMode=JTP"

# Java Paths
export JAVA_HOME={{ pillar['adapt_java_home'] }}/
APARAPI_LIB_PATH={{ pillar['aparapi_folder'] }}
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$APARAPI_LIB_PATH

# Java Paths
JAVA_BIN=${JAVA_HOME}/bin/java

# Build Java ClassPaths
export CWD
source ${CWD}/adapt/classpath-defs.sh

###
# Java Pool Processes
###

for ADAPT_PORT in {% for port in pillar['java_pool_instance_ports'] %}{{ port }} {% endfor %}
do 
	# Log Files
	ARLO_JAVA_LOG=${CWD}/log/arlo.java-port${ADAPT_PORT}-${NOW}.out
	ARLO_JAVA_LOG_CURRENT=${CWD}/log/arlo.java-port${ADAPT_PORT}-current.out
	touch $ARLO_JAVA_LOG
	ln -sf $ARLO_JAVA_LOG $ARLO_JAVA_LOG_CURRENT

	nohup ${JAVA_BIN} -server -Xmx${JAVA_MAX_HEAP} -DARLO_CONFIG_FILE=ArloSettings-pool.properties -DADAPT.HttpCallMeAPIServerPort=${ADAPT_PORT} ${GC_SETTINGS} ${APARAPI_SETTINGS} -classpath ${CLASSPATH} arlo.ServiceHead > $ARLO_JAVA_LOG 2>&1 &
	PID=$!
	echo $PID > ${CWD}/java-port${ADAPT_PORT}.pid
done

###
# QueueRunner Process
###

# Log Files
ARLO_JAVA_LOG=${CWD}/log/arlo.java-queuerunner-${NOW}.out
ARLO_JAVA_LOG_CURRENT=${CWD}/log/arlo.java-queuerunner-current.out
touch $ARLO_JAVA_LOG
ln -sf $ARLO_JAVA_LOG $ARLO_JAVA_LOG_CURRENT

nohup ${JAVA_BIN} -server -Xmx${JAVA_MAX_HEAP} -DARLO_CONFIG_FILE=ArloSettings-QueueRunner.properties ${GC_SETTINGS} ${APARAPI_SETTINGS} -classpath ${CLASSPATH} arlo.ServiceHead > $ARLO_JAVA_LOG 2>&1 &
PID=$!
echo $PID > ${CWD}/java-queuerunner.pid

