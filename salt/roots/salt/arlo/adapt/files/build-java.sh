#!/bin/bash

export JAVA_HOME={{ pillar['adapt_java_home'] }}/
cd {{ pillar['adapt_install_path'] }} && ant -f adapt_ant_build.xml
exit $?
