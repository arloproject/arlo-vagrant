include:
  - arlo.adapt

arlosettings-pool.properties:
  file.managed:
    - require: 
      - git: adapt
      - user: arlo-user
    - name: {{ pillar["adapt_install_path"] }}/ArloSettings-pool.properties
    - source: file://{{ pillar["adapt_install_path"] }}/ArloSettings.properties-TEMPLATE
    - template: jinja
    - mode: 660
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - context:
      DATABASE_NAME: '{{ pillar["arlo_db_name"] }}'
      DATABASE_USER: '{{ pillar["arlo_db_user"] }}'
      DATABASE_PASSWORD: '{{ pillar["arlo_db_password"] }}'
      DATABASE_HOST: '{{ pillar["arlo_db_host"] }}'
      START_HTTPCALLME_SERVER: "true"
      NUM_THREADS: '{{ pillar["adapt_num_threads"] }}'
      HTTPCALLME_PORT: 0 # we'll override this in the startup script
      BIGDATA_CACHE_PATH: '{{ pillar["adapt_bigdata_cache_path"] }}'
      CACHE_ENABLED: '{{ pillar["adapt_cache_enabled"] }}'
      USERFILES_PATH: '{{ pillar["arlo_user_files_path"] }}'
      MEDIAROOT_PATH: '{{ pillar["nester_media_path"] }}'
      QUEUERUNNER_INTERVAL: 0
      QUEUERUNNER_ONESHOT: false
      QUEUERUNNER_ENABLE_IMPORTAUDIO: false
      QUEUERUNNER_ENABLE_TEST: false
      QUEUERUNNER_ENABLE_SUPERVISED_TAG_DISCOVERY_PARENT: false
      QUEUERUNNER_ENABLE_SUPERVISED_TAG_DISCOVERY_CHILD: false
      QUEUERUNNER_ENABLE_UNSUPERVISED_TAG_DISCOVERY: false
      QUEUERUNNER_ENABLE_EXPERT_AGREEMENT_CLASSIFICATION: false
      QUEUERUNNER_ENABLE_WEKAJOB: false
      QUEUERUNNER_ENABLE_LIBRARY_VALIDATION: false
      API_V0_URL: '{{ pillar["arlo_api_v0_url"]|replace(':','\\:') }}'
      QUEUERUNNER_API_USERNAME: {{ pillar['adapt']['queuerunner_api']['username'] }}
      QUEUERUNNER_API_PASSWORD: {{ pillar['adapt']['queuerunner_api']['password'] }}
      QUEUERUNNER_API_HOST: {{ pillar['adapt']['queuerunner_api']['host'] }}
      QUEUERUNNER_API_PORT: {{ pillar['adapt']['queuerunner_api']['port'] }}
      QUEUERUNNER_ONESHOT_MAX_TASKS: 10000
      NESTER_API_USERNAME: {{ pillar['adapt']['queuerunner_api']['username'] }}
      NESTER_API_TOKEN: {{ pillar['adapt']['queuerunner_api']['token'] }}
      NESTER_API_NO_VERIFY_SSL: false
      

arlosettings-queuerunner.properties:
  file.managed:
    - require: 
      - git: adapt
      - user: arlo-user
    - name: {{ pillar["adapt_install_path"] }}/ArloSettings-QueueRunner.properties
    - source: file://{{ pillar["adapt_install_path"] }}/ArloSettings.properties-TEMPLATE
    - template: jinja
    - mode: 660
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - context:
      DATABASE_NAME: '{{ pillar["arlo_db_name"] }}'
      DATABASE_USER: '{{ pillar["arlo_db_user"] }}'
      DATABASE_PASSWORD: '{{ pillar["arlo_db_password"] }}'
      DATABASE_HOST: '{{ pillar["arlo_db_host"] }}'
      START_HTTPCALLME_SERVER: "false"
      QUEUERUNNER_INTERVAL: 10
      NUM_THREADS: '{{ pillar["adapt_num_threads"] }}'
      HTTPCALLME_PORT: 0
      BIGDATA_CACHE_PATH: '{{ pillar["adapt_bigdata_cache_path"] }}'
      CACHE_ENABLED: '{{ pillar["adapt_cache_enabled"] }}'
      USERFILES_PATH: '{{ pillar["arlo_user_files_path"] }}'
      MEDIAROOT_PATH: '{{ pillar["nester_media_path"] }}'
      QUEUERUNNER_ONESHOT: false
      QUEUERUNNER_ENABLE_IMPORTAUDIO: true
      QUEUERUNNER_ENABLE_TEST: true
      QUEUERUNNER_ENABLE_SUPERVISED_TAG_DISCOVERY_PARENT: true
      QUEUERUNNER_ENABLE_SUPERVISED_TAG_DISCOVERY_CHILD: true
      QUEUERUNNER_ENABLE_UNSUPERVISED_TAG_DISCOVERY: true
      QUEUERUNNER_ENABLE_EXPERT_AGREEMENT_CLASSIFICATION: true
      QUEUERUNNER_ENABLE_WEKAJOB: true
      QUEUERUNNER_ENABLE_LIBRARY_VALIDATION: true
      API_V0_URL: '{{ pillar["arlo_api_v0_url"]|replace(':','\\:') }}'
      QUEUERUNNER_API_USERNAME: {{ pillar['adapt']['queuerunner_api']['username'] }}
      QUEUERUNNER_API_PASSWORD: {{ pillar['adapt']['queuerunner_api']['password'] }}
      QUEUERUNNER_API_HOST: {{ pillar['adapt']['queuerunner_api']['host'] }}
      QUEUERUNNER_API_PORT: {{ pillar['adapt']['queuerunner_api']['port'] }}
      QUEUERUNNER_ONESHOT_MAX_TASKS: 10000
      NESTER_API_USERNAME: {{ pillar['adapt']['queuerunner_api']['username'] }}
      NESTER_API_TOKEN: {{ pillar['adapt']['queuerunner_api']['token'] }}
      NESTER_API_NO_VERIFY_SSL: false

