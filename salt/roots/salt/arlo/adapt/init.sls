include:
  - arlo
  - arlo.java
  - .settings

adapt-pkgs:
  pkg.installed:
    - pkgs:
      - ant
      - git
    - require_in: 
      - git: adapt

adapt-folder:
  file.directory:
    - require: 
      - file: arlo-path
      - user: arlo-user
    - name: {{ pillar["adapt_install_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770
    - makedirs: True

adapt:
  git.latest:
    - require:
      - user: arlo-user
      - file: adapt-folder
      - pkg: adapt-pkgs
    - name: {{ pillar["adapt_git_repo"] }}
    - target: {{ pillar["adapt_install_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - force_clone: true
    - force_checkout: True
    - force_reset: True

haproxy:
  pkg:
    - installed
  service: 
    - running
    - watch: 
      - file: haproxy-config
      - file: haproxy-enable-config

haproxy-config:
  file.managed:
    - name: /etc/haproxy/haproxy.cfg
    - source: salt://arlo/adapt/files/haproxy.cfg
    - template: jinja
    - makedirs: True
    - mode: 644

haproxy-enable-config:
  file.managed:
    - name: /etc/default/haproxy
    - source: salt://arlo/adapt/files/etc_default_haproxy
    - mode: 644

start-java.sh:
  file.managed:
    - require: 
      - file: adapt-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/start-java.sh
    - source: salt://arlo/adapt/files/start-java.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

build-java.sh:
  file.managed:
    - require: 
      - file: adapt-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/build-java.sh
    - source: salt://arlo/adapt/files/build-java.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

stop.sh:
  file.managed:
    - require:
      - file: adapt-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/stop.sh
    - source: salt://arlo/adapt/files/stop.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

clean-java.sh:
  file.managed:
    - require:
      - file: adapt-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/clean-java.sh
    - source: salt://arlo/adapt/files/clean-java.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

sox-pkg:
  pkg.installed:
    - pkgs:
      - sox
      - libsox-fmt-mp3

bigdata-cache:
  file.directory:
    - require: 
      - file: arlo-path
      - user: arlo-user
    - name: {{ pillar["adapt_bigdata_cache_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770
    - makedirs: True

