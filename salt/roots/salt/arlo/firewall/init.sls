# based on http://blog.publysher.nl/2013/08/infra-as-repo-securing-your.html

ufw:
  pkg:
    - installed
  service:
    - running
    - require:
      - cmd: ufw-enable

# Enable
ufw-enable:
  cmd.run:
    - name: ufw -f enable
    - require:
      - pkg: ufw
      - cmd: ufw-ssh
      - cmd: ufw-mysql
      - cmd: ufw-apache
      - cmd: ufw-django-runserver-port

# SSH
ufw-ssh:
  cmd.run:
    - name: ufw allow 22
    - require:
      - pkg: ufw

# MySQL
ufw-mysql:
  cmd.run:
    - name: ufw allow 3306
    - require:
      - pkg: ufw

# open service ports
ufw-apache:
  cmd.run:
    - name: ufw allow {{ pillar['apache_http_port'] }}
    - require:
      - pkg: ufw

ufw-django-runserver-port:
  cmd.run:
    - name: ufw allow {{ pillar['nester_runserver_port'] }}
    - require:
      - pkg: ufw

