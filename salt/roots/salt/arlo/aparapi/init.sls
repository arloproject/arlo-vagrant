include:
  - arlo

aparapi-folder:
  file.directory:
    - require: 
      - user: arlo-user
    - name: {{ pillar["aparapi_folder"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770
    - recurse:
        - user
        - group
        - mode
    - makedirs: True


aparapi:
  archive.extracted:
    - require:
      - file: aparapi-folder
    - name: {{pillar["aparapi_extract_folder"] }}
#    - source: "salt://arlo/aparapi/Aparapi_2013_01_23_linux_x86_64.zip"
    - source: "salt://arlo/aparapi/aparapi-2012-05-06.zip"
    - archive_format: zip
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
#    - archive_format: tar
#    - tar_options: z
    - if_missing: {{pillar["aparapi_jar"] }}


# OpenCL Library necessary for aparapi-2012-05-06

aparapi-packages:
  pkg.installed:
    - names:
      - nvidia-libopencl1-340

aparapi-opencl-link:
  file.symlink:
    - name: {{ pillar["aparapi_folder"] }}/libOpenCL.so
    - target: /usr/lib/x86_64-linux-gnu/libOpenCL.so.1
    - requires:
      - pkg: aparapi-packages
      - file: aparapi-folder
