###
# Java 7

#java-packages:
#  pkg.installed:
#    - pkgs:
#      - openjdk-7-jdk


###
# Java 8

java-ppa-repo:
  pkgrepo.managed:
    - humanname: OpenJDK Repo
    - ppa: openjdk-r/ppa

java-packages:
  pkg.installed:
    - pkgs:
      - openjdk-8-jdk
    - require:
      - pkgrepo: java-ppa-repo
