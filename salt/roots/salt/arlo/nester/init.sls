include:
  - arlo

###
# System Packages

nester-packages:
  pkg.installed:
    - names:
      - python-dev
      - python-virtualenv
      - python-pip
      - libmysqlclient-dev
      - git
      - mysql-client-5.5
      - python-mysqldb
    - require_in: 
      - git: nester

###
# Virtualenv

nester-virtualenv:
  virtualenv.managed:
    - name: {{ pillar['nester_virtualenv_path'] }}
    - system_site_packages: False
    - user: {{ pillar['arlo_user'] }}
    - requirements: {{ pillar["nester_install_path"] }}/requirements.txt
    - require:
      - pkg: nester-packages
      - git: nester

nester-folder:
  file.directory:
    - require: 
      - file: arlo-path
      - user: arlo-user
    - name: {{ pillar["nester_install_path"] }}
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 770
    - makedirs: True

nester:
  git.latest:
    - require:
      - user: arlo-user
      - file: nester-folder
      - pkg: nester-packages
    - name: {{ pillar['nester']['git']['repo_url'] }}
    {% if 'branch' in pillar['nester']['git'] %}
    - rev: {{ pillar['nester']['git']['branch'] }}
    {% endif %}
    - target: {{ pillar["nester_install_path"] }}
    - user: {{ pillar["arlo_user"] }}
#    - onlyif: /bin/false   # To effectively disable updates, uncomment
    - force_clone: true     # this first line, and comment the next three
    - force_checkout: True
    - force_reset: True

local-settings-py:
  file.managed:
    - require:
      - git: nester
      - user: arlo-user
    - name: {{ pillar["nester_install_path"] }}/settings.py
    - source: file://{{ pillar["nester_install_path"] }}/settings.template
    - template: jinja
    - mode: 660
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - context: 
      adaptCallHost: {{ pillar['java_pool_host'] }}
      adaptCallPort: {{ pillar['java_pool_port'] }}
      adaptCallPath: {{ pillar['java_pool_path'] }}
      ADAPT_HOST: {{ pillar['java_pool_host'] }}
      ADAPT_PORT: {{ pillar['java_pool_port'] }}
      ADAPT_PATH: {{ pillar['java_pool_path'] }}
      NESTER_ROOT: '{{ pillar["nester_install_path"] }}'
      MEDIA_ROOT: '{{ pillar["nester_media_path"] }}'
      USER_FILE_ROOT: '{{ pillar["arlo_user_files_path"] }}'
      FILE_UPLOAD_TEMP_DIR: '{{ pillar["nester_media_path"] }}/files/tmp/'
      APACHE_ROOT: '{{ pillar["apache_web_protocol"] }}://{{ pillar["arlo_hostname"] }}:{{ pillar["apache_http_port"] }}'
      URL_PREFIX: ''
      MEDIA_URL: '{{ pillar["apache_media_url"] }}/'
      STATIC_URL: '{{ pillar["nester_static_url"] }}/'
      DATABASE_NAME: '{{ pillar["arlo_db_name"] }}'
      DATABASE_USER: '{{ pillar["arlo_db_user"] }}'
      DATABASE_PASSWORD: '{{ pillar["arlo_db_password"] }}'
      DATABASE_HOST: '{{ pillar["arlo_db_host"] }}'
      DATABASE_PORT: ''
      STATIC_ROOT: '{{ pillar["nester_static_path"] }}'
      ALLOWED_HOSTS: {{ pillar["nester_allowed_hosts"] }}
      ARLO_API_V0_URL: '{{ pillar["arlo_api_v0_url"] }}'
      NESTER_SECRET_KEY: '{{ pillar["nester_secret_key"] }}'

nester-wsgi-config:
  file.managed:
    - require: 
      - git: nester
      - user: arlo-user
      - virtualenv: nester-virtualenv
    - name: {{ pillar['nester_install_path'] }}/{{ pillar['django_wsgi_file'] }}
    - source: salt://arlo/nester/files/django.wsgi
    - template: jinja
    - makedirs: True
    - mode: 664
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

nester-wsgi-config-touch:
  file.touch:
      - require:
        - file: nester-wsgi-config
      - name: {{ pillar['nester_install_path'] }}/{{ pillar['django_wsgi_file'] }}

start-django-server:
  file.managed:
    - require: 
      - file: nester-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/start-django-server.sh
    - source: salt://arlo/nester/files/start-django-server.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

nester-start-queuerunner-script:
  file.managed:
    - require: 
      - file: nester-folder
      - user: arlo-user
    - name: {{ pillar["arlo_base_path"] }}/start-python-queuerunner.sh
    - source: salt://arlo/nester/files/start-python-queuerunner.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}


# Run Migrate on the databaase: 

nester-migratedb-file:
  file.managed:
    - require: 
      - user: arlo-user
      - git: nester
      - file: local-settings-py
      - mysql_grants: arlo-db-user-privs
      - virtualenv: nester-virtualenv
    - name: {{ pillar["arlo_base_path"] }}/migrate-db.sh
    - source: salt://arlo/nester/files/migrate-db.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

nester-migratedb-run:
  cmd.run:
    - name: {{ pillar["arlo_base_path"] }}/migrate-db.sh
    - user: {{ pillar["arlo_user"] }}
    - require:
      - file: nester-migratedb-file

# Update Static Files

nester-updatestatic-file:
  file.managed:
    - require: 
      - file: nester-migratedb-file
    - name: {{ pillar["arlo_base_path"] }}/update-static.sh
    - source: salt://arlo/nester/files/update-static.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

nester-updatestatic-run:
  cmd.run:
    - name: {{ pillar["arlo_base_path"] }}/update-static.sh
    - user: {{ pillar["arlo_user"] }}
    - require:
      - file: nester-updatestatic-file

# Docs Build Script

nester-docs-packages:
  pkg.installed:
    - names:
      - graphviz

nester-docs-virtualenv:
  virtualenv.managed:
    - name: {{ pillar['nester_docs_virtualenv_path'] }}
    - system_site_packages: False
    - user: {{ pillar['arlo_user'] }}
    - requirements: {{ pillar["nester_install_path"] }}/doc/requirements.txt
    - require:
      - pkg: nester-docs-packages
      - pkg: nester-packages
      - git: nester

nester-docsbuildscript-file:
  file.managed:
    - require: 
      - virtualenv: nester-docs-virtualenv
    - name: {{ pillar["arlo_base_path"] }}/build-docs.sh
    - source: salt://arlo/nester/files/build-docs.sh
    - template: jinja
    - mode: 770
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}

nester-docsbuildscript-run:
  cmd.run:
    - name: {{ pillar["arlo_base_path"] }}/build-docs.sh
    - user: {{ pillar["arlo_user"] }}
    - require:
      - file: nester-docsbuildscript-file
