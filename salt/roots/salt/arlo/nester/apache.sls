include:
  - arlo
  - arlo.nester

apache-pkg:
  pkg: 
    - installed
    - name: apache2

# use a separate service config to work around possible bug
# https://github.com/saltstack/salt/issues/2852
apache-service:
  service.running:
    - enable: True
    - name: apache2
    - full_restart: True
    - watch: 
      - file: arlo-apache-vhost
      - file: arlo-apache-vhost-symlink
      - file: nester-wsgi-config
    - require: 
      - pkg: apache-pkg
      - pkg: nester-packages

# Make Apache a member of arlo's group
apache-group:
  user.present:
    - name: www-data
    - require: 
      - pkg: apache-pkg
      - user: arlo-user
    - groups:
      - {{ pillar['arlo_user'] }}

mod-wsgi:
  pkg: 
    - installed
    - require: 
      - pkg: apache-pkg
    - name: libapache2-mod-wsgi

arlo-apache-vhost:
  file.managed:
    - require: 
      - pkg: apache-pkg
      - pkg: mod-wsgi
      - user: apache-group
    - name: /etc/apache2/sites-available/arlo.conf
    - source: salt://arlo/nester/files/arlo-vhost.conf
    - template: jinja
    - mode: 644

arlo-apache-vhost-symlink:
  file.symlink:
    - require:
      - file: arlo-apache-vhost
    - name: /etc/apache2/sites-enabled/arlo.conf
    - target: /etc/apache2/sites-available/arlo.conf
