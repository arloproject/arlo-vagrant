#!/bin/bash

# 'migrate' the Django database

source {{ pillar['nester_virtualenv_path'] }}/bin/activate

cd {{ pillar['nester_install_path'] }}
python manage.py migrate


