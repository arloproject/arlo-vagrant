#!/bin/bash

# Build the Nester Docs in-place

source {{ pillar['nester_docs_virtualenv_path'] }}/bin/activate

cd {{ pillar['nester_install_path'] }}/doc/ && make clean && make html
