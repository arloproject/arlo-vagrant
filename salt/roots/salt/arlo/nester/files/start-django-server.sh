#!/bin/bash

# This starts up the Django development server. 

source {{ pillar['nester_virtualenv_path'] }}/bin/activate

HOST="{{ pillar['arlo_hostname'] }}"
NESTER_PORT={{ pillar['nester_runserver_port'] }}

# Log Files
NOW=$(date +"%d%b%y.%H%M%S")
ARLO_PY_LOG={{ pillar['arlo_log_path'] }}/arlo.python-${NOW}.out
ARLO_PY_LOG_CURRENT={{ pillar['arlo_log_path'] }}/arlo.python-current.out
touch $ARLO_PY_LOG
ln -sf $ARLO_PY_LOG $ARLO_PY_LOG_CURRENT

##
# Start Nester (Django/Python Site)
cd {{ pillar['nester_install_path'] }}
# refresh static/ files
rm -rf {{ pillar['nester_static_path'] }}/*
python manage.py collectstatic --noinput
# Start Server
nohup python manage.py runserver ${HOST}:${NESTER_PORT} > $ARLO_PY_LOG 2>&1 &
PID=$!
echo $PID > {{ pillar['arlo_base_path'] }}/python.pid

