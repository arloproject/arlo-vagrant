#!/bin/bash

# This starts up the Python based QueueRunner daemon.

source {{ pillar['nester_virtualenv_path'] }}/bin/activate
HOST="{{ pillar['arlo_hostname'] }}"

# Log Files
NOW=$(date +"%d%b%y.%H%M%S")
ARLO_PY_LOG={{ pillar['arlo_log_path'] }}/arlo.python-queuerunner-${NOW}.out
ARLO_PY_LOG_CURRENT={{ pillar['arlo_log_path'] }}/arlo.python-queuerunner-current.out
touch $ARLO_PY_LOG
ln -sf $ARLO_PY_LOG $ARLO_PY_LOG_CURRENT

##
# Start Python QueueRunner

cd {{ pillar['nester_install_path'] }}
export DJANGO_SETTINGS_MODULE='settings'
export PYTHONPATH='{{ pillar['nester_install_path'] }}'

nohup python {{ pillar['nester_install_path'] }}/utils/QueueRunner/QueueRunner.py  > $ARLO_PY_LOG 2>&1 &
PID=$!
echo $PID > {{ pillar['arlo_base_path'] }}/python-queuerunner.pid
