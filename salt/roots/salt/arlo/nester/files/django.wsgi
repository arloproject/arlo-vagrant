import os
import sys
import site

# Add the site-packages of the virtualenv
site.addsitedir('{{ pillar['nester_virtualenv_path'] }}/local/lib/python2.7/site-packages')

sys.path.append('{{ pillar["arlo_base_path"] }}')
sys.path.append('{{ pillar["nester_install_path"] }}')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

# Activate the  virtualenv
activate_env=os.path.expanduser("{{ pillar['nester_virtualenv_path'] }}/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
