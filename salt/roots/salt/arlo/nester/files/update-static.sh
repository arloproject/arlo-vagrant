#!/bin/bash

# Update the Django static files

source {{ pillar['nester_virtualenv_path'] }}/bin/activate

cd {{ pillar['nester_install_path'] }}
# refresh static/ files
rm -rf {{ pillar['nester_static_path'] }}/*
python manage.py collectstatic --noinput
