mysql-pkgs:
  pkg:
    - installed
    - names:
      - mysql-client-5.5
      - python-mysqldb
    - require_in: 
      - mysql_grants: arlo-db-user-privs

mysql-server:
  pkg:
    - installed
    - require:
      - pkg: mysql-pkgs
    - name: mysql-server-5.5
    - require_in: 
      - mysql_grants: arlo-db-user-privs
      - mysql_database: arlo-db
      - mysql_user: arlo-db-user
  service:
    - require: 
      - pkg: mysql-server
    - running
    - enable: True
    - name: mysql
    - watch:
      - file: mysql-mycnf

mysql-mycnf:
  file.replace:
    - name: /etc/mysql/my.cnf
    - append_if_not_found: True
    - pattern: "bind-address.*127.0.0.1.*"
    - repl: "bind-address = 0.0.0.0"
    - require:
      - pkg: mysql-server

arlo-db-user:
  mysql_user.present:
    - name: {{ pillar['arlo_db_user'] }}
    - password: {{ pillar['arlo_db_password'] }}
    - host: "%"
    - require: 
      - pkg: mysql-server
      - service: mysql-server

arlo-db:
  mysql_database.present:
    - name: {{ pillar['arlo_db_name'] }}
    - require: 
      - pkg: mysql-server
      - service: mysql-server

arlo-db-user-privs:
  mysql_grants.present:
    - require: 
      - mysql_user: arlo-db-user
      - mysql_database: arlo-db
      - pkg: mysql-pkgs
      - service: mysql-server
    - grant: all privileges
    - database: {{ pillar['arlo_db_name'] }}.*
    - user: {{ pillar['arlo_db_user'] }}
    - host: "%"

