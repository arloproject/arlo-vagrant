# A collection of various utilities pre-installed for convenience

include:
  - arlo

# Run System Upgrade

util-upgrade:
  pkg.uptodate:
    - refresh: True


utilities:
  pkg.installed:
    - pkgs: 
      - curl
      - git
      - ncdu
      - ntp
      - ntpdate
      - pv
      - screen
      - telnet
      - tree
      - vim

minion-service:
  service.dead:
    - enable: False
    - name: salt-minion
    - require_in:
      - sls: arlo

pylintrc-file:
  file.managed:
    - name: /home/arlo/.pylintrc
    - require:
      - user: arlo-user
    - source: salt://utils/files/pylintrc
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 644

bash_aliases:
  file.managed:
    - name: /home/arlo/.bash_aliases
    - require:
      - user: arlo-user
    - source: salt://utils/files/bash_aliases
    - user: {{ pillar["arlo_user"] }}
    - group: {{ pillar["arlo_user"] }}
    - mode: 644

