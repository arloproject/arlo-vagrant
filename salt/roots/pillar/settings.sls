######################
#  General Settings  #
######################

# The system user ARLO will run as.
arlo_user: arlo

# ARLO Hostnames
# As a convenience, *.vagrantdev.arloproject.com resolves to 192.168.99.9
# this can be used for any internal testing. Update this as applicable
# for non-development installs.
arlo_hostname: vagrantdev.arloproject.com
arlo_media_hostname: media.vagrantdev.arloproject.com

###################
#  ARLO Database  #
###################

arlo_db_host: vagrantdev.arloproject.com
arlo_db_name: arlo
arlo_db_user: arlo
arlo_db_password: password

#############
#  Folders  #
#############

# Where files are located on the system.
# no trailing slash

arlo_base_path: /opt/arlo
arlo_log_path: /opt/arlo/log
adapt_install_path: /opt/arlo/adapt
nester_install_path: /opt/arlo/nester
nester_virtualenv_path: /opt/arlo/nester-virtualenv
nester_media_path: /opt/arlo/nester/media
nester_static_path: /opt/arlo/nester/static
arlo_user_files_path: /opt/arlo/user-files
nester_docs_virtualenv_path: /opt/arlo/nester-docs-virtualenv
nester_docs_build_path: /opt/arlo/nester/doc/build/html


####################
#  Adapt Settings  #
####################

adapt_git_repo: https://bitbucket.org/arloproject/arlo-adapt.git

#NOTE this will be dependent upon the Java version and OS
#adapt_java_home: /usr/lib/jvm/java-7-openjdk-amd64 
adapt_java_home: /usr/lib/jvm/java-8-openjdk-amd64

adapt_java_max_heap: 2G # -Xmx${adapt_java_max_heap}
adapt_bigdata_cache_path: /opt/arlo/bigdata_cache
adapt_num_threads: 2
adapt_cache_enabled: false

adapt:
  queuerunner_api:
    username: arlo
    password: arlo
    token: "UPDATEME"
    host: vagrantdev.arloproject.com
    port: 80

# Java Pool
java_pool_host: '0.0.0.0'
java_pool_port: 10100
java_pool_path: '/callMe/'
java_pool_instance_ports:
  - 10098
  - 10099

# Aparapi
aparapi_extract_folder: /opt/aparapi
aparapi_folder: /opt/aparapi/aparapi-2012-05-06
aparapi_jar: /opt/aparapi/aparapi-2012-05-06/aparapi.jar


#####################
#  Nester Settings  #
#####################

nester:
  git:
    repo_url: https://bitbucket.org/arloproject/arlo-nester.git
#    branch: master  # If not specified, defaults to 'master' or use current branch if exists

nester_runserver_port: 9000

# Apache
apache_http_port: 80
# e.g., 'http'
apache_web_protocol: http
# relative to nester_install_path
django_wsgi_file: django.wsgi
# with protocol
apache_media_url: 'http://media.vagrantdev.arloproject.com'
nester_static_url: 'http://vagrantdev.arloproject.com/static'
arlo_api_v0_url: 'http://vagrantdev.arloproject.com/api/v0/'

nester_secret_key: 'em=5_le&l)*1@hv$ah2u65&p@5yi$_+$=4imlwagkaqnn#y6!_'

# A list of strings representing the host/domain names that this Django site can
#  serve. 
#
#  # To disable, and allow ALL hosts, use the following 
#  # (should only be used in development, not on production sites)
#  # ALLOWED_HOSTS = ['*']
nester_allowed_hosts:
  - '.vagrantdev.arloproject.com'
  - '.vagrantdev.arloproject.com.'


