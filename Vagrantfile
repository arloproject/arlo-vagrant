# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "precise64"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"
#  config.vm.box = "ubuntu/trusty64"


  # As a convenience, *.vagrantdev.arloproject.com resolves to 192.168.99.9
  # this can be used for any internal testing. 
  config.vm.network :private_network, ip: "192.168.99.9"


  ##
  # VirtualBox Parameters

  config.vm.provider :virtualbox do |vb|
    # boot with headless mode
    vb.gui = false
    vb.customize ["modifyvm", :id, "--memory", "1200"]
  end


  ###
  # Sync'd folders

  # Sync /opt/arlo/ to local arlo/ - see docs/syncing_files_to_host.rst for more information
#  config.vm.synced_folder "arlo/", "/opt/arlo", owner: 1001, group: 1003, mount_options: ["dmode=770"]


  ##
  # Salt-Cachier
  # Setup auto package caching

#  config.cache.auto_detect = true

  if Vagrant.has_plugin?("vagrant-cachier")
    # Configure cached packages to be shared between instances of the same base box.
    # More info on http://fgrehm.viewdocs.io/vagrant-cachier/usage
    config.cache.scope = :box

    # If you are using VirtualBox, you might want to use that to enable NFS for
    # shared folders. This is also very useful for vagrant-libvirt if you want
    # bi-directional sync
#    config.cache.synced_folder_opts = {
#      type: :nfs,
#      # The nolock option can be useful for an NFSv3 client that wants to avoid the
#      # NLM sideband protocol. Without this option, apt-get might hang if it tries
#      # to lock files needed for /var/cache/* operations. All of this can be avoided
#      # by using NFSv4 everywhere. Please note that the tcp option is not the default.
#      mount_options: ['rw', 'vers=3', 'tcp', 'nolock']
#    }
  end

  ##
  # Salt Provisioning 

#  # more workarounds for screwed up Salt Bootstrap issues
#  config.vm.provision "shell",
#    inline: "sudo apt-get install -y python-pip python-software-properties && sudo add-apt-repository -y ppa:chris-lea/python-urllib3"
#
  # Pre-install some packages to work around Salt dependency hell
  config.vm.provision "shell",
    inline: "sudo -- sh -c 'export DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get install -y mysql-server mysql-client-5.5 python-mysqldb git'"

  # For masterless, mount your salt file root
  config.vm.synced_folder "salt/roots/", "/srv/"

  config.vm.provision :salt do |salt|
    salt.masterless = true
    salt.version = "2015.8.10"
    salt.minion_config = "salt/roots/minion"
    salt.run_highstate = true
    salt.bootstrap_options = "-P" # Enable pip-based installation
    # temporarily use our own copy of this script, as a recent change has 
    # broken the hosted version: https://github.com/saltstack/salt-bootstrap/issues/632
#    salt.bootstrap_script = "bootstrap-salt.sh"  
  end


    ##############################
    #  Define Multiple Machines  #
    ##############################

#  config.vm.define "nester" do |nester|
#    # As a convenience, *.vagrantdev.arloproject.com resolves to 192.168.99.9
#    # this can be used for any internal testing. 
#    nester.vm.network :private_network, ip: "192.168.99.9"
#  end
#
#  config.vm.define "adapt" do |adapt|
#
#    adapt.vm.network :private_network, ip: "192.168.99.10"
#
#    adapt.vm.provision :salt do |salt|
#      salt.masterless = true
#      salt.version = "2015.8.10"
#      salt.minion_config = "salt/roots/minion-adapt"
#      salt.run_highstate = true
#      salt.bootstrap_options = "-P" # Enable pip-based installation
#    end
#
#  end

end
