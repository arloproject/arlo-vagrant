Running Nester in a Debug Webserver
===================================

This guide walks through setting up a Debug webserver for running the
Nester project. The debug webserver facilitates development and testing
by:

* Running as a single process, rather than from a pool
* Allows pdb or similar debuggers and interactive breakpoints
* Automatically reloads most code changes, rather than caching them.

Background
----------

When using the Apache webserver, code is cached and changes may not be
readily noticed. In fact, as the requests hit various pool processes, which
will reload on different schedules, the changes may seem to come and go.

The better option is to run out of Django's built-in webserver, which
alleviates most of these issues.

Setup
-----

Follow these steps to start the debug webserver.

    # Login to the Vagrant box
    vagrant ssh

    # Switch to the arlo user
    sudo -u arlo -i

    # Load the ARLO Virtualenv Environment
    source /opt/arlo/nester-virtualenv/bin/activate

    # go to the ARLO directory
    cd /opt/arlo/nester/

    # manually refresh static/ files
    rm -rf /opt/arlo/nester/static/*
    python manage.py collectstatic --noinput

    # Start the server
    python manage.py runserver vagrantdev.arloproject.com:9000

This will run the webserver in the foreground of the terminal. Note
you'll have to manually update the hostname and/or port that the server
will use.

Once running, you can connect at http://vagrantdev.arloproject.com:9000

Ctrl-C to stop the server.

Limitations
-----------

Note that the debug server will not automatically load all changes. For
sure, these are cases where additional steps may be needed:

* Changing Static Files (images, JavaScript, etc. in static/ directories)

* Changing core Django files, such as settings.py, models.py, or urls.py

* Any changes requiring a migration will require a wholly separate process
  (e.g., if changing data definitions).

When in doubt of changes appearing, re-run the steps above to refresh the
static files (JavaScript, images, etc.) and restart the server.
