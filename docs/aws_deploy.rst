Deploying on AWS
================

This guide walks through creating an ARLO installation on an AWS server,
and optionally adding one or multiple additional Java Job Processing
servers. While we'll manually create the VMs, installation and
configuration will be semi-automated by the Salt configurations.

You'll need a few items to proceed:

* Amazon AWS account and basic knowledge of creating EC2 nodes.

* A domain name that you can point hostnames to your server

  * We'll need to point two domains at your Nester server, the *hostname*
    you'll use to access it, as well as *media.hostname*. For example,

    * vagrantdev.arloproject.com
    * media.vagrantdev.arloproject.com  


Planning our Architecture
-------------------------

ARLO can be installed on only a single server, though for better
performance during intensive tasks, it is usually preferred to have
numerous servers available to share the tasks.

The actual deployments for ARLO can vary widely to match use cases or
available resources. This demonstrated deployment is not necessarily
a best practice rather meant to provide a practical starting point.

With this setup, we are assuming a moderately sized deployment that
will have:

* A 'Master' node which contains:

  * the database to store all user-generated data and metadata

  * the **Nester** web interface, through which users will connect

  * the *interactive* Adapt Java processes, which will import files and
    generate spectra for display on the web interface

* Any number of *Job Processing* servers, which will run the Adapt Java
  *QueueRunner* processes.

  * These servers will communicate to the 'Master' server to retrieve
    tasks and run the search jobs.

  * These servers will need access to both the HTTP web API (*Nester*)
    as well as being able to connect to the database.

  * These servers can be added or removed as needed to scale to use.


Recommended Minimums
~~~~~~~~~~~~~~~~~~~~

The following are recommended minimums from my own testing, though
the exact sizes needed will depend largely on your own datasets.

* Nester/Adapt/Database Server

  * t2 series probably fine
  
  * Public IP Address - I'd recommend an Elastic IP address here to ensure
    it remains consistent if stopped / restarted.

  * Memory

    * 2 GB RAM minimum, more may be required depending on Max MediaFile size

    * just guessing, target 2x the size of the Max single file size

  * Disk

    * I'd recommend EBS so that it persists if you stop the instance
    * SSD
    * probably 20 GB fine for a small test

* Adapt Job Runners

  * Same as above, except,

  * as much CPU as practical


Region
~~~~~~

This guide assumes all Amazon resources will be created in the same
region within Amazon's network. While the defaults are probably sane,
throughout the process check the region settings while adding resources.


File Sharing
~~~~~~~~~~~~

Probably the most difficult consideration for planning an ARLO
deployment is sharing the MediaFiles between all of the servers.
While all user-generated data and MediaFile Metadata is contained
within the database and remotely accessible, ARLO does not internally
provide a direct method for sharing files.

Consideration needs to be given to the large amount of data that
this system may be accessing. Given the large amount of data being
read by each node during a large search, some network file stores may
not keep up with the load.

In this deployment, we will utilize Amazon's Elastic File System
(EFS) to create a shared volume between the servers.


Create the Nester 'Master' Node
-------------------------------

Security Group
~~~~~~~~~~~~~~

For ease of Network configuration and security, we'll create a new
*Security Group* for the ARLO deployment. All ARLO servers will be
assigned to this Group.

* From the EC2 Console, select **Security Groups**

* Create a new Security Group

  * Add a name and Description, such as "ARLO"

  * Select the appropriate *VPC* (likely the default)

  * Add the following **Inbound** rules:

    * Type: SSH / Source: Anywhere

    * Type: HTTP / Source: Anywhere

    * Type: HTTPS / Source: Anywhere

  * Select **Create** to save the Security Group

  * Edit Rules

    * Copy the **Group ID** for the new group

    * Right-Click on the new group, and select "Edit inbound rules"

    *  Add a new Rule to allow MySQL Traffic between all of the Nodes:

      * Type: MySQL/Aurora

      * Source: Custom

      * In the right-most box, paste the Group ID copied above

    *  Add a new Rule to allow NFS Traffic:

      * Type: NFS

      * Source: Custom

      * In the right-most box, paste the Group ID copied above


Create a Ubuntu 14.04 Instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* From the EC2 Dashboard, select "Launch Instance"

* Select a Ubuntu 14.04 AMI (such as "Ubuntu Server 14.04 LTS (HVM), SSD
  Volume Type")

* Select the Type and Size (see Recommended Minimums above) - a good
  starting point for a small test is a t2.small

* Step 3:

  * You'll likely use the defaults. However, as you add additional servers,
    ensure that they are in the same Subnets / Networks, etc.

* Step 4:

  * (see Recommended Minimums above) - for example, a 20 GB on "General
    Purpose SSD (GP2)"

* Step 5:

  * Add a name (e.g., "Arlo-Nester")

* Step 6

  * Select the existing Security Group you created above.

* Review and Launch

* Optional

  * If using Elastic IPs, you can now add or edit the IP and assign it to
    the new instance.


Install SaltStack
~~~~~~~~~~~~~~~~~

* Login to your instance::

    ssh ubuntu@<ip_address>

* sudo to root::

    sudo -i

* install requirements::

    export DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get install -y mysql-server mysql-client-5.5 python-mysqldb git

* download the bootstrap script::

    wget -O bootstrap_salt.sh https://bootstrap.saltstack.com

* install the specific version of SaltStack::

    sh bootstrap_salt.sh -P git v2015.8.10


Copy the ARLO SaltStack Scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Checkout the ARLO-Vagrant Repo::

    git clone https://bitbucket.org/arloproject/arlo-vagrant.git

* Copy the appropriate minion config file

  * **minion** is configured to install Nester, Adapt, and the Database::

      cp arlo-vagrant/salt/roots/minion /etc/salt/

  * **minion-adapt** is configured to install Adapt (e.g., for Job Runners)::

      cp arlo-vagrant/salt/roots/minion-adapt /etc/salt/minion

* Copy the files into /srv::

    cp -ar arlo-vagrant/salt/roots/* /srv/

* Update the Settings to match your installation::

    vim /srv/pillar/settings.sls

  * The following settings need to be adjusted to the Nester 'master' hostname.

    * arlo_hostname

    * arlo_media_hostname

    * adapt -> queuerunner_api -> host

    * apache_media_url

    * nester_static_url

    * arlo_api_v0_url

    * nester_allowed_hosts

  * Database

      If installing on the Nester host, set the *host* the the selected
      hostname. If using an external database, adjust accordingly.


Run Salt
~~~~~~~~

Run Salt's *highstate* to provision::

    salt-call -l debug --local state.highstate

Look for any failed states.


Common Issues
^^^^^^^^^^^^^

* On any state failures, I'd recommend re-running Salt once to see if
  there was a temporary issue that can be automatically fixed. 


Mount Elastic File System
~~~~~~~~~~~~~~~~~~~~~~~~~

Create
^^^^^^

If you haven't already, create a new Elastic File System from the
AWS Console. (https://docs.aws.amazon.com/efs/latest/ug/getting-started.html)

* Make sure the VPC matches that of your EC2 servers

* Check the Security Groups for the mount targets match that of your
  EC2 servers

* General Purpose I/O (default) should be sufficient

* Make note of the File System ID that you've created.

**Note** Check that your VPC has DNS enabled to ensure the following mount
procedures will work. 

To enable this feature:

* go to the Amazon VPC console

* in the Actions menu for your VPC, select "Edit DNS Resolution" and
  "Edit DNS Hostnames" and make sure both are set to "Yes".


Mount
^^^^^

We'll follow Amazon's guide for mounting the filesystem
(https://docs.aws.amazon.com/efs/latest/ug/gs-step-three-connect-to-ec2-instance.html)

Repeat this process for each of your servers:

* Login to your instance::

    ssh ubuntu@<ip_address>

* sudo to root::

    sudo -i

* Install NFS tools::

    apt-get install nfs-common

* Mount the EFS volume to /opt/arlo/user-files/

    mount -t nfs4 -o vers=4.1 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).FILE-SYSTEM-ID.efs.AWS-REGION.amazonaws.com:/ /opt/arlo/user-files

  * Replace *FILE-SYSTEM-ID* with the ID for your EFS volume

  * Replace *AWS-REGION* with the region from your EC2 instance

    * For example, *us-east-1* (not *us-east-1a*, drop the last letter)

* Chown the resulting directory to *arlo*

    chown arlo. /opt/arlo/user-files


**Note** this mount procedure will need to be re-ran every time the server
is restarted.


Finalize Nester Setup
~~~~~~~~~~~~~~~~~~~~~


Database and Superuser Creation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Before using ARLO, we need to build the database and create an Admin user.
This Admin user will only be used for creating normal users later. 

* Login to the server, and change to the Arlo user::

    sudo -u arlo -i

* Create an ARLO Admin User::

    cd /opt/arlo/nester

    source ../nester-virtualenv/bin/activate

    python manage.py createsuperuser

* Login to ARLO as the Admin user, and create a new user (since this admin
  user was not created from ARLO, it won't have an appropriate configuration
  for using ARLO, only administering). Use a URL similar to the following,
  adjusting to your VM's hostname:: 

    http://vagrantdev.arloproject.com/tools/userHome


Configuring Adapt API
^^^^^^^^^^^^^^^^^^^^^

Now we'll need to setup the ARLO internal System user and configure the
authentication settings for the Adapt Settings.

First, we need to generate an API Token for the Admin user.
The easiest way is through the Django Admin interface.

* Navigate to http://vagrantdev.arloproject.com/admin/ (rather the appropriate
  URL for your installation).

* Click on 'Tokens'

* Click 'Add Token'

* Select the Admin user you previously created.

* Select 'Save', and copy the Token for the next step.

Once we have the Token, we need to:

* Login to the *root* user again

* Open the Salt Pillar settings::

    vim /srv/pillar/settings.sls

* Update the API Token (adapt -> queuerunner_api -> token)

* Rerun Salt::

    salt-call -l debug --local state.highstate


Starting ARLO Adapt (Java)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Become the ARLO user

    sudo -u arlo -i

* Build and Start ADAPT Java Pool and QueueRunner

    cd /opt/arlo

    ./clean.sh  # Ignore errors on first run

    ./build-java.sh

    ./start-java.sh

* If you want to tail the Java logs:: 

    tail -f /opt/arlo/log/arlo.java-*current.out


Deploying Additional Adapt Nodes
--------------------------------

Deploying additional **Adapt** Nodes for use as Job runners follows the
same procedure as above for the Nester node, with the following
differences:

* Skip these steps:

  * In *Copy the ARLO SaltStack Scripts* section:

    * use the '*minion-adapt*' configuration file instead of 'minion'

    * Be sure to use the Nester hostname (of the master node) in the
      configurations.

  * Copy the Adapt API Token into the settings file before the first
    run of Salt (since we have that available beforehand this time).

  * When configuring the /srv/pillar/settings.sls file, for the database
    hostname, instead use the internal IP address of the 'Master' Nester
    server. This can be obtained by running 'ip a' on the Nester server.
    For example, in the following, the IP address is 172.30.0.66::

        2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc pfifo_fast state UP group default qlen 1000
            link/ether 0a:fd:b1:dc:18:ad brd ff:ff:ff:ff:ff:ff
            inet 172.30.0.66/24 brd 172.30.0.255 scope global eth0
               valid_lft forever preferred_lft forever
            inet6 fe80::8fd:b1ff:fedc:18ad/64 scope link 
               valid_lft forever preferred_lft forever

  * Skip the *Finalize Nester Setup* sections.


Security
--------

This installation makes no significant attempts at security. Some things to 
consider:

* MySQL root user will by default have no password
* ARLO database credentials will be readable in the Salt Pillar settings file


Troubleshooting
---------------


