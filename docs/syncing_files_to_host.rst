Syncing Files to the Host Machine
=================================

This guide walks through creating an ARLO installation on a local host
and syncing the /opt/arlo/ files out to the local machine, where an
editor can be used for accessing the files.

This guide assumes you are using the Virtualbox provider with Vagrant.

#. While not strictly necessary, I recommend starting with a fresh Vagrant,
   (*vagrant destroy*) but you'll need to at minimum power off the
   VM (*vagrant halt*).

#. Create an empty directory *arlo/* in the *arlo-vagrant* folder. If the
   directory exists, you may want to empty, or note that running
   provisioning may modify it's contents (if you have changes, they may
   be overwritten with the public repositories).

#. Uncomment the Synced Folder configuration in *Vagrantfile*

    config.vm.synced_folder "arlo/", "/opt/arlo", owner: 1001, group: 1003, mount_options: ["dmode=770"]

#. *vagrant up* to start the box.

#. Manually Re-run the Salt provisioning.

    # Login to the Vagrant box

    vagrant ssh

    # Switch to root

    sudo -i

    # run the Salt provisioning

    salt-call --local state.highstate

**Note** One of the Salt states may fail (permissions failure on the
MediaFile symlink) and this can safely be ignored.

Files in /opt/arlo on the VM should now be available in the arlo/
directory on the host.

